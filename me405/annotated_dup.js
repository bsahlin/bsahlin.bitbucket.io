var annotated_dup =
[
    [ "encoder_example", null, [
      [ "Encoder", "classencoder__example_1_1Encoder.html", "classencoder__example_1_1Encoder" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "FSM_BalancePlatform", null, [
      [ "taskBalancePlatform", "classFSM__BalancePlatform_1_1taskBalancePlatform.html", "classFSM__BalancePlatform_1_1taskBalancePlatform" ]
    ] ],
    [ "MCP9808", null, [
      [ "MCP9808", "classMCP9808_1_1MCP9808.html", "classMCP9808_1_1MCP9808" ]
    ] ],
    [ "motor_example", null, [
      [ "Motor", "classmotor__example_1_1Motor.html", "classmotor__example_1_1Motor" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "StateSpaceController", null, [
      [ "StateSpaceController", "classStateSpaceController_1_1StateSpaceController.html", "classStateSpaceController_1_1StateSpaceController" ]
    ] ],
    [ "TouchPanelDriver", null, [
      [ "TouchPanel", "classTouchPanelDriver_1_1TouchPanel.html", "classTouchPanelDriver_1_1TouchPanel" ]
    ] ],
    [ "tpd", null, [
      [ "TouchPanel", "classtpd_1_1TouchPanel.html", "classtpd_1_1TouchPanel" ]
    ] ]
];