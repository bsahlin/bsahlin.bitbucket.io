var lab6main_8py =
[
    [ "backend_run_interval", "lab6main_8py.html#a6ead5e0e3cadc56820376dd1e04dcfc8", null ],
    [ "ch1_number", "lab6main_8py.html#a895a67df9a1b2771b15bf51878f4ed57", null ],
    [ "ch1_pin_name", "lab6main_8py.html#a367a8fdbfbd6664c59df3a9f092f9118", null ],
    [ "ch2_number", "lab6main_8py.html#a1b0d6759ebf8c70967a40f499ff896ca", null ],
    [ "ch2_pin_name", "lab6main_8py.html#abc2760d156ece99811cba3335fe23c00", null ],
    [ "collect_run_interval", "lab6main_8py.html#ae6509794b1ff11d7d7b2fa917e8d400b", null ],
    [ "collect_time", "lab6main_8py.html#a784439b480ba36f64bd122204e95867d", null ],
    [ "encoder1", "lab6main_8py.html#abf59306b1f4e21f5c2bb67969ff93664", null ],
    [ "Kp", "lab6main_8py.html#a655708966972be05555b0e86332bac83", null ],
    [ "motor1", "lab6main_8py.html#acfd6aa731fde797abd6d5b7200fa591c", null ],
    [ "motortimer", "lab6main_8py.html#a5541a6ccb8421294a01b3d4abf616a42", null ],
    [ "pin_IN1", "lab6main_8py.html#a4ea08eb291d2ae2b90e80a799377e3c1", null ],
    [ "pin_IN2", "lab6main_8py.html#aecc323a82f27df27b390155f12d546f5", null ],
    [ "pin_nSLEEP", "lab6main_8py.html#a04d5f8dd711e5db60121faacf1f8a308", null ],
    [ "PropController1", "lab6main_8py.html#ab0170e7ba3bebcd14610c1bc7cc7e1bb", null ],
    [ "sat_max", "lab6main_8py.html#a18001b72d0056fd77f2f8c3c651210f5", null ],
    [ "sat_min", "lab6main_8py.html#a6294a781b29aa2094f7d651fe1f77ba1", null ],
    [ "taskBackEnd1", "lab6main_8py.html#afbd7e70466d37811dd81c159102bc6e8", null ],
    [ "taskCLController1", "lab6main_8py.html#af915a859431188a064666b87340bd3ff", null ],
    [ "ticks_per_deg", "lab6main_8py.html#a84f2cc652d934c7c4c346bfcf2d8d069", null ],
    [ "timer_number", "lab6main_8py.html#aed03826b11f043b669d2bb4c79cb63d3", null ],
    [ "usbv1", "lab6main_8py.html#a76eaae71a6944b33e42ad6a37987bc6a", null ],
    [ "Vref", "lab6main_8py.html#a4ac75fad546ab850a6e172fdecd35cd2", null ]
];