var files_dup =
[
    [ "BLE.py", "BLE_8py.html", [
      [ "BLE", "classBLE_1_1BLE.html", "classBLE_1_1BLE" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "FSM_BackEnd.py", "FSM__BackEnd_8py.html", [
      [ "taskBackEnd", "classFSM__BackEnd_1_1taskBackEnd.html", "classFSM__BackEnd_1_1taskBackEnd" ]
    ] ],
    [ "FSM_CLController.py", "FSM__CLController_8py.html", [
      [ "taskCLController", "classFSM__CLController_1_1taskCLController.html", "classFSM__CLController_1_1taskCLController" ]
    ] ],
    [ "FSM_datagen.py", "FSM__datagen_8py.html", [
      [ "taskDataGen", "classFSM__datagen_1_1taskDataGen.html", "classFSM__datagen_1_1taskDataGen" ]
    ] ],
    [ "FSM_Elevator.py", "FSM__Elevator_8py.html", [
      [ "TaskElevator", "classFSM__Elevator_1_1TaskElevator.html", "classFSM__Elevator_1_1TaskElevator" ],
      [ "Button", "classFSM__Elevator_1_1Button.html", "classFSM__Elevator_1_1Button" ],
      [ "MotorDriver", "classFSM__Elevator_1_1MotorDriver.html", "classFSM__Elevator_1_1MotorDriver" ]
    ] ],
    [ "FSM_LED.py", "FSM__LED_8py.html", [
      [ "taskLED", "classFSM__LED_1_1taskLED.html", "classFSM__LED_1_1taskLED" ]
    ] ],
    [ "FSM_LEDControl.py", "FSM__LEDControl_8py.html", [
      [ "taskLEDControl", "classFSM__LEDControl_1_1taskLEDControl.html", "classFSM__LEDControl_1_1taskLEDControl" ]
    ] ],
    [ "FSM_UI.py", "FSM__UI_8py.html", [
      [ "taskUI", "classFSM__UI_1_1taskUI.html", "classFSM__UI_1_1taskUI" ]
    ] ],
    [ "lab3main.py", "lab3main_8py.html", "lab3main_8py" ],
    [ "lab5main.py", "lab5main_8py.html", "lab5main_8py" ],
    [ "lab6main.py", "lab6main_8py.html", "lab6main_8py" ],
    [ "lab7main.py", "lab7main_8py.html", "lab7main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_hw0.py", "main__hw0_8py.html", "main__hw0_8py" ],
    [ "main_lab2.py", "main__lab2_8py.html", "main__lab2_8py" ],
    [ "main_lab4.py", "main__lab4_8py.html", "main__lab4_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "PropController.py", "PropController_8py.html", [
      [ "PropController", "classPropController_1_1PropController.html", "classPropController_1_1PropController" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "shares6.py", "shares6_8py.html", "shares6_8py" ],
    [ "shares7.py", "shares7_8py.html", "shares7_8py" ],
    [ "ui_front.py", "ui__front_8py.html", "ui__front_8py" ],
    [ "ui_front_PropControl.py", "ui__front__PropControl_8py.html", "ui__front__PropControl_8py" ],
    [ "ui_front_PropControl_lab7.py", "ui__front__PropControl__lab7_8py.html", "ui__front__PropControl__lab7_8py" ]
];