/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Labs", "index.html", [
    [ "List of Assignments", "index.html#sec_labsList", null ],
    [ "Lab 00", "page_lab00.html", [
      [ "Introduction", "page_lab00.html#sec_intro", null ],
      [ "Motor Driver", "page_lab00.html#sec_mot", null ],
      [ "Encoder Driver", "page_lab00.html#sec_enc", null ]
    ] ],
    [ "Lab 01", "page_lab01.html", [
      [ "Introduction", "page_lab01.html#sec_lab1_intro", null ],
      [ "Source Code", "page_lab01.html#sec_Lab1_source", null ],
      [ "Files", "page_lab01.html#sec_lab1_files", null ]
    ] ],
    [ "HW 00", "page_HW00.html", [
      [ "Introduction", "page_HW00.html#sec_HW00_intro", null ],
      [ "FSM Diagram", "page_HW00.html#sec_HW00_FSMdiagram", null ],
      [ "Source Code", "page_HW00.html#sec_HW00_source", null ],
      [ "Files", "page_HW00.html#sec_HW00_files", null ]
    ] ],
    [ "Lab 02", "page_lab02.html", [
      [ "Introduction", "page_lab02.html#sec_Lab02_intro", null ],
      [ "FSM Diagram", "page_lab02.html#sec_Lab02_FSMdiagram", null ],
      [ "LED Patterns", "page_lab02.html#sec_Lab02_pattern", null ],
      [ "Source Code", "page_lab02.html#sec_Lab02_source", null ],
      [ "Files", "page_lab02.html#sec_Lab02_files", null ]
    ] ],
    [ "Lab 03", "page_Lab03.html", [
      [ "Introduction", "page_Lab03.html#sec_Lab03_intro", null ],
      [ "FSM Diagrams", "page_Lab03.html#sec_Lab03_FSMdiagram", null ],
      [ "Source Code", "page_Lab03.html#sec_Lab03_source", null ],
      [ "Files", "page_Lab03.html#sec_Lab03_files", null ]
    ] ],
    [ "Lab 04", "page_Lab04.html", [
      [ "Introduction", "page_Lab04.html#sec_Lab4_intro", null ],
      [ "FSM Diagrams", "page_Lab04.html#sec_Lab04_FSMdiagram", null ],
      [ "Task Diagram", "page_Lab04.html#sec_Lab04_TaskDiagram", null ],
      [ "Source Code", "page_Lab04.html#sec_Lab04_source", null ],
      [ "Files", "page_Lab04.html#sec_Lab04_files", null ]
    ] ],
    [ "Lab 05", "page_Lab05.html", [
      [ "Introduction", "page_Lab05.html#sec_Lab5_intro", null ],
      [ "FSM Diagram", "page_Lab05.html#sec_Lab05_FSMdiagram", null ],
      [ "iPhone App", "page_Lab05.html#sec_Lab05_app", null ],
      [ "Task Diagram", "page_Lab05.html#sec_Lab05_taskdiagram", null ],
      [ "Source Code", "page_Lab05.html#sec_Lab05_source", null ],
      [ "Files", "page_Lab05.html#sec_Lab05_files", null ]
    ] ],
    [ "Lab 06", "page_Lab06.html", [
      [ "Introduction", "page_Lab06.html#sec_Lab6_intro", null ],
      [ "Pulse Width Modulation Scheme", "page_Lab06.html#sec_Lab6_PWMdiagram", null ],
      [ "Task Diagram", "page_Lab06.html#sec_Lab6_TaskDiagram", null ],
      [ "Finite State Machine Diagrams", "page_Lab06.html#sec_Lab6_FSMs", null ],
      [ "Step Response Plots", "page_Lab06.html#sec_Lab6_StepResponses", null ],
      [ "Source Code", "page_Lab06.html#sec_Lab6_source", null ],
      [ "Files", "page_Lab06.html#sec_lab6_files", null ]
    ] ],
    [ "Lab 07", "page_Lab7.html", [
      [ "Introduction", "page_Lab7.html#sec_Lab7_intro", null ],
      [ "Pulse Width Modulation Scheme", "page_Lab7.html#sec_Lab7_PWMdiagram", null ],
      [ "Task Diagram", "page_Lab7.html#sec_Lab7_TaskDiagram", null ],
      [ "Finite State Machine Diagrams", "page_Lab7.html#sec_Lab7_FSMs", null ],
      [ "Response Plot", "page_Lab7.html#sec_Lab7_Responses", null ],
      [ "Source Code", "page_Lab7.html#sec_Lab7_source", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"lab5main_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';