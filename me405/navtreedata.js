/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Labs", "index.html", [
    [ "List of Assignments", "index.html#sec_labsList", null ],
    [ "Lab 00", "page_Lab00.html", [
      [ "Introduction", "page_Lab00.html#sec_intro", null ],
      [ "Motor Driver", "page_Lab00.html#sec_mot", null ],
      [ "Encoder Driver", "page_Lab00.html#sec_enc", null ],
      [ "Source Code", "page_Lab00.html#sec_Lab0_source", null ]
    ] ],
    [ "Lab 01", "page_lab01.html", [
      [ "FSM Diagram", "page_lab01.html#sec_fsm", null ]
    ] ],
    [ "Lab 02", "page_Lab02.html", null ],
    [ "Lab 03", "page_lab03.html", [
      [ "Response Plot", "page_lab03.html#sec_lab3_plot", null ]
    ] ],
    [ "Lab 04", "page_Lab04.html", [
      [ "Temperature Plots", "page_Lab04.html#sec_lab4plots", null ],
      [ "Source Code", "page_Lab04.html#sec_lab4source", null ]
    ] ],
    [ "Lab 05", "page_lab05.html", [
      [ "Hand Calculations", "page_lab05.html#sec_handcalcs", null ],
      [ "Matlab Program", "page_lab05.html#sec_matlab", null ]
    ] ],
    [ "Lab 06", "page_lab06.html", [
      [ "Response Plots", "page_lab06.html#sec_plots", null ]
    ] ],
    [ "Lab 07", "page_lab07.html", [
      [ "Picture and Screenshot", "page_lab07.html#sec_lab7_pics", null ]
    ] ],
    [ "Lab 08", "page_Lab08.html", null ],
    [ "Lab 09", "page_lab09.html", [
      [ "Video Explanation and Demonstation", "page_lab09.html#sec_video", null ],
      [ "System Photos", "page_lab09.html#sec_platformphotos", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"main4_8py.html#aa7d7955a9f4cb1a16184595bcd01f874"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';