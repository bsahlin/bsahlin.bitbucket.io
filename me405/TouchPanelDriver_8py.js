var TouchPanelDriver_8py =
[
    [ "TouchPanel", "classTouchPanelDriver_1_1TouchPanel.html", "classTouchPanelDriver_1_1TouchPanel" ],
    [ "interval", "TouchPanelDriver_8py.html#a07cbcc855454772cbca2d98421a77189", null ],
    [ "panel_center_x", "TouchPanelDriver_8py.html#af9f931f6aea880e840af7909ad034ae1", null ],
    [ "panel_center_y", "TouchPanelDriver_8py.html#a466f1e15416aeaf044a8d7af828fcafe", null ],
    [ "panel_height", "TouchPanelDriver_8py.html#a0921be73e50beea6ab752cb304f1211b", null ],
    [ "panel_width", "TouchPanelDriver_8py.html#a20735b74f99d33d85c17d7c4207e46f4", null ],
    [ "pin_xm", "TouchPanelDriver_8py.html#a62da3236a56745db2f69df035ec756ea", null ],
    [ "pin_xp", "TouchPanelDriver_8py.html#aff003468df501715eb950779df3992d4", null ],
    [ "pin_ym", "TouchPanelDriver_8py.html#a749f84e05c1cb7a1b78fb4f9c900ef71", null ],
    [ "pin_yp", "TouchPanelDriver_8py.html#acdfa1dcbcd6cb9cd4fc9769ebba06d48", null ],
    [ "tp", "TouchPanelDriver_8py.html#a239f9ec8b0a6ca728eb2825b19a301fc", null ]
];