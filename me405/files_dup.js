var files_dup =
[
    [ "encoder_example.py", "encoder__example_8py.html", [
      [ "Encoder", "classencoder__example_1_1Encoder.html", "classencoder__example_1_1Encoder" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", "EncoderDriver_8py" ],
    [ "frontendlab3.py", "frontendlab3_8py.html", "frontendlab3_8py" ],
    [ "FSM_BalancePlatform.py", "FSM__BalancePlatform_8py.html", [
      [ "taskBalancePlatform", "classFSM__BalancePlatform_1_1taskBalancePlatform.html", "classFSM__BalancePlatform_1_1taskBalancePlatform" ]
    ] ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "main9.py", "main9_8py.html", "main9_8py" ],
    [ "mainlab0.py", "mainlab0_8py.html", "mainlab0_8py" ],
    [ "mainlab1.py", "mainlab1_8py.html", "mainlab1_8py" ],
    [ "mainlab2.py", "mainlab2_8py.html", "mainlab2_8py" ],
    [ "mainlab3.py", "mainlab3_8py.html", "mainlab3_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "motor_example.py", "motor__example_8py.html", [
      [ "Motor", "classmotor__example_1_1Motor.html", "classmotor__example_1_1Motor" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "StateSpaceController.py", "StateSpaceController_8py.html", "StateSpaceController_8py" ],
    [ "TouchPanelDriver.py", "TouchPanelDriver_8py.html", "TouchPanelDriver_8py" ],
    [ "tpd.py", "tpd_8py.html", "tpd_8py" ]
];