var frontendlab3_8py =
[
    [ "onKeypress", "frontendlab3_8py.html#a6d6450526924f9c3327a515a3a8f490e", null ],
    [ "plotData", "frontendlab3_8py.html#a2aea4e66f8102af4aa036bdecbe5584a", null ],
    [ "saveCSV", "frontendlab3_8py.html#aa0a65b9488c675feda3314b25dea318c", null ],
    [ "data_array", "frontendlab3_8py.html#a718fb0c4bb245bb1fb2db269347b6a36", null ],
    [ "i", "frontendlab3_8py.html#a94d18baaaa0da591bdc7f269b032ba86", null ],
    [ "pushed_key", "frontendlab3_8py.html#afa766ad9a62d260bff317e4d5ace7374", null ],
    [ "ser", "frontendlab3_8py.html#a957559569fba56166f1b2843d76f8961", null ],
    [ "serial_input", "frontendlab3_8py.html#ab83299fcf34fc94738b156a2968435c0", null ],
    [ "step_array", "frontendlab3_8py.html#ad1dc505263d1a52a72e5032e86d1032a", null ],
    [ "t", "frontendlab3_8py.html#aac1100ab8ce1184359a18580fa75b271", null ],
    [ "v_input", "frontendlab3_8py.html#a70b1741219514572afb47292a01b825c", null ],
    [ "v_output", "frontendlab3_8py.html#a84bd9ce08b8312b7915e63a8371dfb29", null ]
];