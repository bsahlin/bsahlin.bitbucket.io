var EncoderDriver_8py =
[
    [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ],
    [ "e1_ch1_pin", "EncoderDriver_8py.html#a4ff034ab7efbc076faf9eb7a31066e9e", null ],
    [ "e1_ch2_pin", "EncoderDriver_8py.html#a986f7f9199f53b987ef3b1ee92190457", null ],
    [ "e2_ch1_pin", "EncoderDriver_8py.html#a6da2ae883a1e029825103a0814c12810", null ],
    [ "e2_ch2_pin", "EncoderDriver_8py.html#abad37172a4fa2c52c678d68634f28148", null ],
    [ "enc1", "EncoderDriver_8py.html#af343dd92e4586e35593dc0ae4ee33db1", null ],
    [ "enc2", "EncoderDriver_8py.html#a814ca2de118c1274d01243960d9143e0", null ],
    [ "ticks_per_deg", "EncoderDriver_8py.html#a928b974163139b13b189639617f3c8a2", null ],
    [ "tim4", "EncoderDriver_8py.html#a363cad8a4f2a942f08d19a18a42ffc36", null ],
    [ "tim8", "EncoderDriver_8py.html#adb072241d6fb067447561998dae68ce5", null ]
];