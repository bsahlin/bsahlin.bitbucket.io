var tpd_8py =
[
    [ "TouchPanel", "classtpd_1_1TouchPanel.html", "classtpd_1_1TouchPanel" ],
    [ "avg_time", "tpd_8py.html#a325309bf17f6fbaa4993d62193249482", null ],
    [ "panel_center_x", "tpd_8py.html#ad9e9677aedef9a8ea422aece6f916a2a", null ],
    [ "panel_center_y", "tpd_8py.html#ad0c62672ec052539a32736f57d297ced", null ],
    [ "panel_height", "tpd_8py.html#a2d5c8baec08eb4ecaef296d5caee8984", null ],
    [ "panel_width", "tpd_8py.html#af217e4813a0de659e9a8a47395333a1a", null ],
    [ "pin_xm", "tpd_8py.html#a27fe86f82c0015a280f3e1caed7b9899", null ],
    [ "pin_xp", "tpd_8py.html#a590a969c1104644c663768f2fa64e04e", null ],
    [ "pin_ym", "tpd_8py.html#a10fa6deae0b7929b7bc9f23f68e111d3", null ],
    [ "pin_yp", "tpd_8py.html#aa6e817ff592c68e4e8449955c81201a6", null ],
    [ "tp", "tpd_8py.html#a178ef2a40a6fd919af1b5dc97d0b8e6f", null ]
];