var searchData=
[
  ['savecsv_195',['saveCSV',['../ui__front_8py.html#a95739f038358ad4890591a96b04079f8',1,'ui_front.saveCSV()'],['../ui__front__PropControl_8py.html#a3ea9f03f088bde4dae835f2a1fa754da',1,'ui_front_PropControl.saveCSV()'],['../ui__front__PropControl__lab7_8py.html#a2e1480f7964f3822af88449cd697e43f',1,'ui_front_PropControl_lab7.saveCSV()']]],
  ['send_5fref_5fvals_5farray_196',['send_ref_vals_array',['../ui__front__PropControl__lab7_8py.html#a306a1517d45f2ad69c18dfc8875a5b23',1,'ui_front_PropControl_lab7']]],
  ['set_5fduty_5fcycle_197',['set_duty_cycle',['../classmotor_1_1Motor.html#aab6d345560661d43ccdb60a94165cc35',1,'motor::Motor']]],
  ['set_5fkp_198',['set_Kp',['../classPropController_1_1PropController.html#a00505aee2bcdc8fd172b8c8c727f7077',1,'PropController::PropController']]],
  ['set_5fposition_199',['set_position',['../classencoder_1_1encoder.html#ac851a9301451ea951c5f38c3a500473c',1,'encoder.encoder.set_position()'],['../classEncoderDriver_1_1EncoderDriver.html#a06fa8c8d499b314a489959aa304d19cd',1,'EncoderDriver.EncoderDriver.set_position()']]],
  ['setbuttonstate_200',['setButtonState',['../classFSM__Elevator_1_1Button.html#a69b313e5b3e5d18fca3077b057f5ea40',1,'FSM_Elevator::Button']]],
  ['setduty_201',['setDuty',['../classMotorDriver_1_1MotorDriver.html#a3eaeeaffd14cf7fa65d433e979dd8057',1,'MotorDriver::MotorDriver']]],
  ['stop_202',['Stop',['../classFSM__Elevator_1_1MotorDriver.html#abff872e1d7559190a55361e70b4fb27c',1,'FSM_Elevator::MotorDriver']]],
  ['swap_203',['swap',['../classBLE_1_1BLE.html#a9c724542b8107c24d8f74ec4e4d14c7a',1,'BLE::BLE']]]
];
